/*
Nextflow paired read analysis pipeline for the Chimera Project S.cer motif insertion experiment.
This contains the logic to analysis QuantSeq and 5PSeq data depending on input variables.
*/

/*
Define input parameters.
To find where they are used, search the document for the name,
e.g. "params.featurename" is used in the featureCounts call.
*/

/*
Experiment specific parameters such are where the raw fastq files are
and where the pipeline outputs should be saved. The values can be changed here
or by passing a new value to the variable when calling the Nextflow CLI,
e.g. nextflow run paired_reads_pipeline --experiment_name = '5PSeq'
*/
params.experiment_name = 'QuantSeq'
params.combine_reads_over_lanes = false
params.fastq_file_regex = '*_L001_R{1,2}_001.fastq.gz'
params.sample_name_regex = '(?:WT|gal80)[A-C]P?[1-3]_S[0-9]+'
params.input_fq_dir = '/home/yu/rnaseq/'
params.output_dir = '/home/yu/code/rnaseq/output/'
params.read_1_forward = false
params.remove_UMI = false

/*
String of nucleotides representing sequence adapters that should
be trimmed from reads with cutadapts
*/
params.read_reverse_adapter = 'TTTTTTTTTTTTTTTTTT'
params.read_adapters_1 = 'AGATCGGAAGAGCACACGTCTGAACTCCAGTCA'
params.read_adapters_2 = 'AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGT'
params.read_adapters_3 = 'AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT'
params.read_forward_adapter = 'AAAAAAAAAAAAAAAAAA'

/*
Miscellaneous variables
*/
params.multiple_genome_indexes = false
params.index_prefix = '/home/yu/code/rnaseq-paired-end-test/input_annotation/index/genome_tran'
params.featuretype = 'primary_transcript'
params.featurename = 'ID'
params.num_processes = 4
params.index_dir = ''
params.mRNAgff = '/home/yu/code/rnaseq-paired-end-test/input_annotation/longest_full-ORF_ypd_plus_other_fixed_UTR_length_transcripts.gff'
params.mRNAgff_dir = ''

/*
Flatten nested list of file names (used after grouping by sample),
i.e. convert list of list of strings to list of strings.
 */

flattenFileList = {
    list_of_paired_files = it[1]
    flattened_file_list = list_of_paired_files.flatten()
    it[1] = flattened_file_list
    it
}

/* Extract sample code from file name, multi-lane version */
extract_sample_code_multi_lane = {
    filename = it[0]
    sample_name = (filename =~ "^$params.sample_name_regex")[0]
    it[0] = sample_name
    it
}

/* Extract sample code from file name single-lane version */
extract_sample_code_single_lane = {
    sample_name = (it =~ "(?<=$params.input_fq_dir)$params.sample_name_regex")[0]
    tuple sample_name, it
}

/*
Run if the reads from the same sample are spread over multiple flow lanes and
need to be combined, i.e. if you are running the Chimera project QuantSeq analysis.
*/

if(params.combine_reads_over_lanes){
    /*
    Define the input fastq.gz files, pairing forward and reverse reads and grouping across lanes by sample anme
    */

    multi_lane_input_fq = Channel
        .fromFilePairs("${params.input_fq_dir}${params.fastq_file_regex}", size: 2)
        .filter {it[0] =~ "^$params.sample_name_regex"}
        .map(extract_sample_code_multi_lane)
        .groupTuple(size: 4)
        .map(flattenFileList)

    process combineLanesAcrossSamples {
        errorStrategy 'retry'
        maxRetries 3
        tag "${sample_id}"
        input:
        set sample_id, file(seq) from multi_lane_input_fq

        output:
        tuple val(sample_id), file("${sample_id}_R*.fastq.gz") into input_fq

        """
        cat ${seq.findAll{it =~/_R1_/}.asType(nextflow.util.BlankSeparatedList)} > ${sample_id + '_R1.fastq.gz'}
        cat ${seq.findAll{it =~/_R2_/}.asType(nextflow.util.BlankSeparatedList)} > ${sample_id + '_R2.fastq.gz'}
        """
    }
}
/*
Run if the reads from the same sample are in one file,
i.e. if you are running the Chimera project 5PSeq analysis.
*/
else{
    /*
    Define the input fastq.gz files, filtering only relevent files,
    pairing forward and reverse reads.
    */

    input_fq = Channel
        .fromPath("${params.input_fq_dir}${params.fastq_file_regex}")
        .map(extract_sample_code_single_lane)
        .groupTuple(size:2, sort:"true")
}

/* split input_fq into two separate channels */
input_fq
    .tap{input_fq_qc}
    .tap{input_fq_umi}

/*
Run FastQC to produce a quality control report for the input data for every sample
*/

process runFastQC{
    conda 'bioconda::fastqc=0.11.9'
    errorStrategy 'terminate'
    maxRetries 3
    tag "${sample_id}"
    publishDir "${params.output_dir}/FastQC/${sample_id}", mode: 'copy', overwrite: true
    input:
        set sample_id, file(paired_sample_fq) from input_fq_qc

    output:
        file("${sample_id}_fastqc/*.zip") into fastqc_files


    """
    mkdir ${sample_id}_fastqc
    fastqc --outdir ${sample_id}_fastqc \
    -t ${params.num_processes} \
    ${paired_sample_fq}
    """
}

/*
Remove UMIs if present
*/

if(params.remove_UMI){
    process removeUMIs{
        errorStrategy 'retry'
        maxRetries 3
        tag "${sample_id}"
        input:
            set sample_id, file(sample_fq) from input_fq_umi
        output:
            tuple val(sample_id), file("UMI_processed.*.gz") into input_fq_cut
        shell:
            """
            umi_tools extract -I ${sample_fq[0]} --bc-pattern=NNNNNNNN \
                --read2-in=${sample_fq[1]} --stdout=UMI_processed.1.fastq.gz \
                --read2-out=UMI_processed.2.fastq.gz --log=UMI_processed.log
            """
    }
}
else{
    input_fq_umi
        .tap{input_fq_cut}
}



